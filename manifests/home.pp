class users::home {
  file {'/tmp/home': 
    ensure => 'directory',
    owner  => 'elmo',
  }
}
