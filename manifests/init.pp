class users {
  include users::home
  user { 'elmo':
    ensure           => 'present',
    comment          => 'ELMO',
    gid              => 'muppets',
    home             => '/tmp/elmo',
  }
  group { 'muppets' :
    ensure => present,
  }
}
